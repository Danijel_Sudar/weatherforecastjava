package com.danijelsudar.danijel.weatherforecastjava.net;

import com.danijelsudar.danijel.weatherforecastjava.model.CityWeatherData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {
    @GET("data/2.5/find")
    Call<CityWeatherData> citiesInCycle(
            @Query("appid")String APPID,
            @Query("lat")String lat ,
            @Query("lon")String lon,
            @Query("cnt")int cnt,
            @Query("units")String units);

}
