package com.danijelsudar.danijel.weatherforecastjava.model;

public class City{
    private String mID;
    private String mCity;
    private String mDate;
    private boolean mIsChecked;
    private String mImage;
    private String mWeather;

    public City(){
        mIsChecked = false;
    }

    public City(String mID, String mCity, String mDate, boolean mIsChecked, String mImage, String mWeather) {
        this.mID = mID;
        this.mCity = mCity;
        this.mDate = mDate;
        this.mIsChecked = mIsChecked;
        this.mImage = mImage;
        this.mWeather = mWeather;
    }

    public String getID() {
        return mID;
    }

    public void setID(String mID) {
        this.mID = mID;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String mCity) {
        this.mCity = mCity;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public boolean isChecked() {
        return mIsChecked;
    }

    public void setIsChecked(boolean mIsChecked) {
        this.mIsChecked = mIsChecked;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String mImage) {
        this.mImage = mImage;
    }

    public String getWeather() {
        return mWeather;
    }

    public void setWeather(String mWeather) {
        this.mWeather = mWeather;
    }

    @Override
    public String toString() {
        return "DrawerItem{" +
                "mID='" + mID + '\'' +
                ", mCity='" + mCity + '\'' +
                ", mDate='" + mDate + '\'' +
                ", mIsChecked=" + mIsChecked +
                ", mImage='" + mImage + '\'' +
                ", mWeather='" + mWeather + '\'' +
                '}';
    }
}
