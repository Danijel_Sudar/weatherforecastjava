package com.danijelsudar.danijel.weatherforecastjava;
import android.content.Context;
import android.content.DialogInterface;
import android.net.NetworkInfo;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.*;
import com.bumptech.glide.Glide;
import com.danijelsudar.danijel.weatherforecastjava.model.City;
import com.danijelsudar.danijel.weatherforecastjava.model.CityWeatherData;
import com.danijelsudar.danijel.weatherforecastjava.model.DrawerItem;
import com.danijelsudar.danijel.weatherforecastjava.model.ListData;
import com.danijelsudar.danijel.weatherforecastjava.net.APIService;
import com.danijelsudar.danijel.weatherforecastjava.net.ApiUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

public class CitiesFragment extends Fragment {
    private static final String KEY_NAV_MENU = "menu";
    private static final String TAG = "CitiesFragment";
    private APIService mAPIService = null;
    private WFAdapter mAdapter = null;
    private ArrayList<City> listCities = new ArrayList<City>();
    /*
    * header iz Drawer menia gde treba staviti grad i temperaturu
    * */
    private DrawerItem header;
    private RecyclerView recycler_view_cities;
    private ProgressBar progressBar;
    private String APP_KEY = "17e60d04fcefa9570f820757322be07a";
    /*
    * pozicija selektovanog grada
    * */
    private int mSelected = -1;
    private int COUNT_CITIES = 30;

    public static CitiesFragment newInstance(DrawerItem header){
        CitiesFragment fragment = new CitiesFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_NAV_MENU, header);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        header = (DrawerItem) getArguments().getSerializable(KEY_NAV_MENU);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cities, container, false);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        recycler_view_cities = (RecyclerView) v.findViewById(R.id.recycler_view_cities);
        recycler_view_cities.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI();
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(listCities.size() == 0) loadData();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    protected void createNetErrorDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("You need a network connection to use this application. Please turn on mobile network or Wi-Fi in Settings.")
                .setTitle("Unable to connect")
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                startActivity(i);
                            }
                        }
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getActivity().finish();
                            }
                        }
                );
        AlertDialog alert = builder.create();
        alert.show();
    }

    /*
    * ucitava podatke sa interneta
    * */
    private void loadData() {
        progressBar.setVisibility(View.VISIBLE);

        if(!isNetworkAvailable()) {
            createNetErrorDialog();
        }

        mAPIService = ApiUtils.getAPIService();
        mAPIService.citiesInCycle(
                APP_KEY,
                "45.11",
                "19.84",
                COUNT_CITIES,
                "metric"/*,
                cb = callback*/
        ).enqueue(new Callback<CityWeatherData>() {

            @Override
            public void onResponse(Call<CityWeatherData> call, Response<CityWeatherData> response) {
                Log.d(TAG, "posts loaded from API:");

                CityWeatherData item = response.body();
                String selectedCity = header.getTitle();

                Collections.sort(item.getList());

                for(int i=0; i<item.getList().size(); i++){
                    ListData it = item.getList().get(i);
                    if(selectedCity.equals(it.getName())){
                        mSelected = i;
                    }
                    City city = new City(it.getId()+"", it.getName(), it.getData(), selectedCity.equals(it.getName()), it.getWeather().get(0).getImageURL(), it.getMain().getTemp() +"");
                    listCities.add(city);
                }

                updateUI();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<CityWeatherData> call, Throwable t) {
                Log.d(TAG, "error loading from API " + t);
            }
        });
    }

    private void changeHeader(String city, String temperature){
        header.setTitle(city);
        if(temperature.equals(""))
            header.setTemperature("");
        else header.setTemperature(temperature+"°");
    }

    private void updateUI() {
        if(mAdapter == null || recycler_view_cities.getAdapter() == null) {
            mAdapter = new WFAdapter(listCities);
            recycler_view_cities.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    private class WFAdapter extends RecyclerView.Adapter<WFViewHolder> {
        private  ArrayList<City> mCities;

        public WFAdapter(ArrayList<City> cities){
            mCities = cities;
        }

        @Override
        public WFViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new WFViewHolder(parent, LayoutInflater.from(getActivity()));
        }

        @Override
        public void onBindViewHolder(WFViewHolder holder, int position) {
            holder.bind(mCities.get(position));
            SelectRadio(holder,position);
        }

        @Override
        public int getItemCount() {
            return mCities.size();
        }

        private void SelectRadio(RecyclerView.ViewHolder holder, final int position){
            ((WFViewHolder)holder).itemView.findViewById(R.id.city_radio_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mSelected == position){
                        mCities.get(position).setIsChecked(!mCities.get(position).isChecked());
                        notifyItemChanged(position);
                        mSelected = -1;
                        changeHeader("","");
                    }else if(mSelected == -1) {
                        mCities.get(position).setIsChecked(!mCities.get(position).isChecked());
                        notifyItemChanged(position);
                        mSelected = position;
                        changeHeader(mCities.get(position).getCity(), mCities.get(position).getWeather());
                    }else {
                        mCities.get(position).setIsChecked(!mCities.get(position).isChecked());
                        mCities.get(mSelected).setIsChecked(!mCities.get(mSelected).isChecked());
                        notifyItemChanged(position);
                        notifyItemChanged(mSelected);
                        mSelected = position;
                        changeHeader(mCities.get(position).getCity(), mCities.get(position).getWeather());
                    }
                }});
            }

    }


    class WFViewHolder extends
            RecyclerView.ViewHolder{

        private City mCity;
        private TextView city_text_view;
        private TextView time_text_view;
        private RadioButton city_radio_button;
        private ImageView image_weather;

        public WFViewHolder(View itemView) {
            super(itemView);
        }


        public WFViewHolder(ViewGroup parent, LayoutInflater inflater){
            super(inflater.inflate(R.layout.list_cities_item, parent, false));
            city_text_view = (TextView) itemView.findViewById(R.id.city_text_view);
            time_text_view = (TextView) itemView.findViewById(R.id.time_text_view);
            city_radio_button = (RadioButton) itemView.findViewById(R.id.city_radio_button);
            image_weather = (ImageView) itemView.findViewById(R.id.image_weather);
        }

        private void setSelected(){
            city_text_view.setTextColor(getResources().getColor(R.color.colorPrimary));
            time_text_view.setTextColor(getResources().getColor(R.color.colorPrimary));
            itemView.setBackgroundColor(getResources().getColor(R.color.selected));
        }

        private void notSelected(){
            itemView.setBackgroundColor(getResources().getColor(R.color.white));
            city_text_view.setTextColor(getResources().getColor(R.color.text));
            time_text_view.setTextColor(getResources().getColor(R.color.text));

        }

        public void bind(City city){
            mCity = city;
            city_text_view.setText(mCity.getCity());
            city_radio_button.setChecked(mCity.isChecked());
            time_text_view.setText(mCity.getDate());

            if(mCity.isChecked()) setSelected(); else notSelected();

            Glide.with(getActivity()).load(mCity.getImage()).into(image_weather);
            Glide.with(getActivity()).load(mCity.getImage()).into(image_weather);
        }
    }
}
