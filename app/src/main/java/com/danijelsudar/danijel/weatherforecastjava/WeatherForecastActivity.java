package com.danijelsudar.danijel.weatherforecastjava;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.danijelsudar.danijel.weatherforecastjava.model.DrawerItem;

import java.util.ArrayList;

public class WeatherForecastActivity extends AppCompatActivity{

    private static final String ITEMS_KEY = "items_key";
    private static final String TAG = "WeatherForecastActivity";
    private static final String HEADER_ID = "header";
    private static int mSelected = 1;
    private NavDrawerAdapter mAdapter;
    private ArrayList<DrawerItem> navItem;
    private RecyclerView nav_view_recycler_view;
    private DrawerLayout drawer_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_forecast);
        setToggleAndToolbar();
        nav_view_recycler_view = (RecyclerView)findViewById(R.id.nav_view_recycler_view);
        nav_view_recycler_view.setLayoutManager(new LinearLayoutManager(this));

        if (savedInstanceState != null && savedInstanceState.getSerializable(ITEMS_KEY) != null) {
            navItem = (ArrayList<DrawerItem>)savedInstanceState.getSerializable(ITEMS_KEY);
        } else {
             navItem = setMenuItems();
        }

        updateUI();
        Log.d(TAG,"select: "+ mSelected + " size: " + navItem.size() + "novo: "+ navItem.get(mSelected).getItemId());
        selectedItem(navItem.get(mSelected).getItemId());//set default fragment
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ITEMS_KEY, navItem);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
    }

    private void selectedItem(String id) {
        switch (id) {
            case "nav_cities":
                changerFragment(CitiesFragment.newInstance(navItem.get(0)));
                break;

            case "nav_No":
                changerFragment(NoFragment.newInstance());
                break;
        }
    }

    private void setToggleAndToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (slideOffset != 0f)
                    mAdapter.notifyItemChanged(0);
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };
        drawer_layout.setDrawerListener(toggle);
        toggle.syncState();
    }

    private ArrayList<DrawerItem> setMenuItems()  {
        navItem = new ArrayList<DrawerItem>();
        navItem.add(new DrawerItem(HEADER_ID));
        navItem.add(new DrawerItem(R.drawable.ic_menu_city, getResources().getString(R.string.cities_drawer_item), "nav_cities"));
        navItem.add(new DrawerItem(R.drawable.ic_menu_no, "Nepoznata teritorija", "nav_No"));
        navItem.get(mSelected).setIsSelected(true);

        return navItem;
    }

    private void changerFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        Fragment _fragment = fm.findFragmentById(R.id.content_drawer);
        if((_fragment == null) || (_fragment.getClass() != fragment.getClass())) {
            _fragment = fragment;
            fm.beginTransaction().replace(R.id.content_drawer, _fragment).commit();
        }
    }

    private void updateUI() {
        if(mAdapter == null) {
            mAdapter = new NavDrawerAdapter(navItem);
            nav_view_recycler_view.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    private class NavDrawerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        private ArrayList<DrawerItem> mItems;
        private static final int ITEM_TIP_HEADER = 0;
        private static final int ITEM_TIP_NORMAL = 1;

        NavDrawerAdapter(ArrayList<DrawerItem> items){
            mItems = items;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType){
                case ITEM_TIP_HEADER:
                    return new NavHeaderViewHolder(parent, LayoutInflater.from(WeatherForecastActivity.this));
                default:/* ITEM_TIP_NORMAL*/
                    return new NavItemViewHolder(parent, LayoutInflater.from(WeatherForecastActivity.this));

            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            int itemType = getItemViewType(position);

            switch (itemType){
                case ITEM_TIP_HEADER:
                    ((NavHeaderViewHolder)holder).bind(mItems.get(position));
                    break;
                case ITEM_TIP_NORMAL:
                    ((NavItemViewHolder)holder).bind(mItems.get(position));
                    ((NavItemViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(mSelected != position){
                                mItems.get(position).setIsSelected(true);
                                mItems.get(mSelected).setIsSelected(false);
                                notifyItemChanged(position);
                                notifyItemChanged(mSelected);
                                mSelected = position;
                            }

                            selectedItem(mItems.get(position).getItemId());
                            drawer_layout.closeDrawer(GravityCompat.START);
                        }
                    });
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        @Override
        public int getItemViewType(int position) {
            if(mItems.get(position).getItemId().equals(HEADER_ID))
                return ITEM_TIP_HEADER;
            else return ITEM_TIP_NORMAL;
        }
    }

    private class NavItemViewHolder extends RecyclerView.ViewHolder{
        private DrawerItem mItem;
        private TextView title_nav_item;
        private ImageView img_item_nav;

        NavItemViewHolder(ViewGroup parent, LayoutInflater inflater){
            super(inflater.inflate(R.layout.nav_item_normal, parent, false));

            title_nav_item = (TextView) itemView.findViewById(R.id.title_nav_item);
            img_item_nav = (ImageView) itemView.findViewById(R.id.img_item_nav);
        }

        private void setSelected(){
            title_nav_item.setTextColor(getResources().getColor(R.color.colorPrimary));
            img_item_nav.setColorFilter(getResources().getColor(R.color.colorPrimary));
            itemView.setBackgroundColor(getResources().getColor(R.color.selected));
        }

        void bind(DrawerItem item){
            mItem = item;
            title_nav_item.setText(mItem.getTitle());
            img_item_nav.setImageResource(mItem.getIcon());
            if(mItem.isSelected()) setSelected();
        }
    }

    private class NavHeaderViewHolder extends RecyclerView.ViewHolder{
        private TextView city_nav_drawer_header;
        private TextView temperature_nav_drawer_header;

        NavHeaderViewHolder(ViewGroup parent,LayoutInflater inflater){
            super(inflater.inflate(R.layout.nav_header_weather_forecast, parent, false));

            temperature_nav_drawer_header = (TextView) itemView.findViewById(R.id.temperature_nav_drawer_header);
            city_nav_drawer_header = (TextView) itemView.findViewById(R.id.city_nav_drawer_header);
        }

        public void bind(DrawerItem item){
            if( item.getTitle().equals("")) city_nav_drawer_header.setText(getResources().getString(R.string.nav_header_drawer_no_secelect));
            else city_nav_drawer_header.setText(item.getTitle());

            temperature_nav_drawer_header.setText(item.getTemperature());
        }
    }
}
