package com.danijelsudar.danijel.weatherforecastjava.model;

import java.io.Serializable;


public class DrawerItem  implements Serializable {
    private int mIcon;
    private String mTitle;
    private String mTemperature;
    private String mItemId;
    private boolean mIsSelected;

    public DrawerItem(){
        mIsSelected = false;
    }
    public DrawerItem(String mItemId){
        mIsSelected = false;
        mTemperature = "";
        this.mItemId = mItemId;
        this.mTitle = "";
    }
    public DrawerItem(int mIcon, String mTitle, String mItemId){
        mIsSelected = false;
        this.mIcon = mIcon;
        this.mTitle = mTitle;
        this.mItemId = mItemId;
    }

    public int getIcon() {
        return mIcon;
    }

    public void setIcon(int mIcon) {
        this.mIcon = mIcon;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getTemperature() {
        return mTemperature;
    }

    public void setTemperature(String mTemperature) {
        this.mTemperature = mTemperature;
    }

    public String getItemId() {
        return mItemId;
    }

    public void setItemId(String mItemId) {
        this.mItemId = mItemId;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setIsSelected(boolean mIsSelected) {
        this.mIsSelected = mIsSelected;
    }

    @Override
    public String toString() {
        return "City{" +
                "mIcon=" + mIcon +
                ", mTitle='" + mTitle + '\'' +
                ", mTemperature='" + mTemperature + '\'' +
                ", mItemId='" + mItemId + '\'' +
                ", mIsSelected=" + mIsSelected +
                '}';
    }
}
