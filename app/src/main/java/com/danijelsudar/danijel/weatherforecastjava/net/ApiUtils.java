package com.danijelsudar.danijel.weatherforecastjava.net;

public class ApiUtils {
    private static final String BASE_URL = "http://api.openweathermap.org/";

    public static APIService getAPIService(){
        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
